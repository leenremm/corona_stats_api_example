# README #

Author: Leen Remmelzwaal
Email: support@corona-stats.mobi
License: CC-BY

### License pertaining to corona-stats data ###

Data license: Open Data Commons Attribution license (ODC-BY)
More information: https://opendatacommons.org/licenses/by/1-0/index.html

### What is this repository for? ###

This repository contains examples of how to draw a Python chart using data from corona-stats.co.za.

### How do I get an API key? ###

Visit corona-stats.mobi, and request an API key (link in footer).

### What is new in JSON.2.0?

1. The URL has changed: https://corona-stats.mobi/api/json.2.0.php?key=YOUR_KEY_HERE

2. We are now publishing the following extended data set:

   * National (Cases, Deaths, Recoveries, Linearized Recoveries, Tests)
   * Provincial (Cases, Deaths, Recoveries)
   * Currency (exchange rates) for EURO, USD, GBP
   * Economy (Repo rate)
  
3. To handle this volume of data, there is now an extended lookup system

   * e.g.case_history = dictionary["RSA"]["National"]["Cases"]

4. Finally, data is stored in string format, and needs to be converted to float / int.

### How do I run the script? ###

1) Open a Command Prompt (windows) or Terminal (Linux) and navigate to the folder containing script.

2) Run the script as follows:

```
cls && python chart.py
```

### How does this script do? ###

The script pulls data from the corona-stats.mobi API, and generates a chart:

![picture](screenshot.png)
