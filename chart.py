import pycurl
from io import BytesIO
import json
import matplotlib.pyplot as plt

b_obj = BytesIO()
crl = pycurl.Curl()
key = "YOUR_KEY_HERE" # Request an API key from corona-stats.co.za
crl.setopt(crl.URL, 'https://corona-stats.mobi/api/json.2.0.php?key=%s' % key)
crl.setopt(crl.WRITEDATA, b_obj)
crl.perform()
crl.close()
get_body = b_obj.getvalue()
dictionary = json.loads(get_body)

# If there is an error in the call
if (dictionary["status"][0] != 200):
    print ("\nError: %s" % dictionary["status"][1])
    exit()

# If successful
x = dictionary["RSA"]["Dates"]

fig, ax1 = plt.subplots()

# Cases
y1 = [int(i) for i in dictionary["RSA"]["National"]["Cases"]]
line, = ax1.plot(x, y1, color='blue')
line.set_label('Cases')
ax1.scatter(x, y1, s=50, color='blue')

# Deaths
y2 = [int(i) for i in dictionary["RSA"]["National"]["Deaths"]]
line, = ax1.plot(x, y2, color='red')
line.set_label('Deaths')
ax1.scatter(x, y2, s=50, color='red')

# Recoveries
y3 = [int(i) for i in dictionary["RSA"]["National"]["Recoveries"]]
line, = ax1.plot(x, y3, color='orange')
line.set_label('Recoveries')
ax1.scatter(x, y3, s=50, color='orange')
ax1.legend(loc=0)

ax2 = ax1.twinx()

# Tests
y4 = [int(i) for i in dictionary["RSA"]["National"]["Tests"]]
line, = ax2.plot(x, y4, color='green')
line.set_label('Total Tests')
ax2.scatter(x, y4, s=50, color='green')
ax2.legend(loc=1)

# General Chart settings
plt.title('SA Corona Virus Stats')
ax1.set_ylabel('Cases, Deaths, Recoveries')
ax1.set_xticklabels(x, rotation='vertical')

plt.show()
